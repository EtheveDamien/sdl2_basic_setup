#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdbool.h>

// gcc src\main.c -Iinclude -Llib -lmingw32 -lSDL2main -lSDL2 -o test

int main(int argc, char *argv[])
{
  SDL_Event event;
  int quit = 0;

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    printf("Impossible d'initialiser la SDL \n");
    return -1;
  }

  SDL_Window *window = SDL_CreateWindow(
    "DamiXIO - sdl game",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    640, 
    480,
    SDL_WINDOW_SHOWN
  );

  if (window == NULL) {
    printf("Impossible de créer la fenetre: %s \n", SDL_GetError());
    return -1;
  }

  SDL_Renderer *renderer = SDL_CreateRenderer(
    window,
    -1,
    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
  );

  if (renderer == NULL) {
    printf("Impossible de créer la renderer: %s \n", SDL_GetError());
    return -1;
  }

  SDL_SetRenderDrawColor(renderer, 50,50,50,255);
  
  while (!quit)
  {
    while( SDL_PollEvent(&event) ){
      switch( event.type ){
          case SDL_KEYDOWN:
            break;

          case SDL_KEYUP:
            break;

          /* SDL_QUIT event (window close) */
          case SDL_QUIT:
            quit = 1;
            break;

          default:
            break;
      }
    }

    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
  }

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}
